'use strict';

const changeThemeButton = document.querySelector('.change-theme');
const body = document.body;

// Змінна для зберігання вибраної теми
let currentTheme = localStorage.getItem('theme') || 'light-theme';

// Встановлюємо початкову тему
setTheme(currentTheme);

// Додаємо обробник події на кнопку "Змінити тему"
changeThemeButton.addEventListener('click', () => {
    // Змінюємо тему
    currentTheme = currentTheme === 'light-theme' ? 'dark-theme' : 'light-theme';
    // Зберігаємо вибрану тему
    localStorage.setItem('theme', currentTheme);
    // Встановлюємо нову тему
    setTheme(currentTheme);
});

// Функція для встановлення теми
function setTheme(theme) {
    body.classList.remove('light-theme', 'dark-theme');
    body.classList.add(theme);
}
